function validateForm(){
    var firstName = document.forms["myForm"]["firstName"].value;
    var lName = document.forms["myForm"]["lName"].value;
    var phone = document.forms["myForm"]["phone"].value;
    var email = document.forms["myForm"]["email"].value;
    var input = document.forms["myForm"]["input"].value;
    if(firstName === null || firstName === ""){
        alert("first name is required");
        return false;
    }
    if(lName === null || lName === ""){
        alert("last name is required");
        return false;
    }
    if(phone === null || phone === ""){
        alert("phone number is required");
        return false;
    }
    if(email === null || email === ""){
        alert("email is required");
        return false;
    }
    if(input === null || input === ""){
        alert("select one course");
        return false;
    }
    if(phone.length < 11) {
        alert("phone number of eleven digits");
        return false;
    }
 }

 function togleSideBar(){
    document.getElementById("sidebar").classList.toggle('active');
}